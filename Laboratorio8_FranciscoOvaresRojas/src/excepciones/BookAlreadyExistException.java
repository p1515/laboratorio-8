package excepciones;

/**
 * Clase BookAlreadyExistException.
 * @author Francisco Javier Ovares Rojas.
 */
public class BookAlreadyExistException extends Exception{
  /** Constructor de Excepcion. Sin parametros. */
  public BookAlreadyExistException() {}
  /**
   *  Constructor de Excepcion. Con parametros.
   * @param pMensajeError String que representa el mensaje de error personalizado a mostrar en consola.
   */
  public BookAlreadyExistException(String pMensajeError) {
    super(pMensajeError);
  }
}
