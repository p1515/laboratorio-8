package excepciones;

/**
 * Clase AuthorAlreadyExistException.
 * @author Francisco Javier Ovares Rojas.
 */
public class AuthorAlreadyExistException extends Exception{
  /** Constructor de Excepcion. Sin parametros. */
  public AuthorAlreadyExistException() {}
  /**
   *  Constructor de Excepcion. Con parametros.
   * @param pMensajeError String que representa el mensaje de error personalizado a mostrar en consola.
   */
  public AuthorAlreadyExistException(String pMensajeError) {
      super(pMensajeError);
  }
}
