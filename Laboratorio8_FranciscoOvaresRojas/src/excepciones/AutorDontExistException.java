package excepciones;

/**
 * Clase AutorDontExistException.
 * @author Francisco Javier Ovares Rojas.
 */
public class AutorDontExistException extends Exception{
  /** Constructor de Excepcion. Sin parametros. */
  public AutorDontExistException() {}
  /**
   *  Constructor de Excepcion. Con parametros.
   * @param pMensajeError String que representa el mensaje de error personalizado a mostrar en consola.
   */
  public  AutorDontExistException(String pMensajeError) {
    super(pMensajeError);
  }
}
