package excepciones;

/**
 * Clase ClientAlreadyExistException.
 * @author Francisco Javier Ovares Rojas.
 */
public class ClientAlreadyExistException extends Exception{
  /** Constructor de Excepcion. Sin parametros. */
  public ClientAlreadyExistException() {}
  /**
   *  Constructor de Excepcion. Con parametros.
   * @param pMensajeError String que representa el mensaje de error personalizado a mostrar en consola.
   */
  public ClientAlreadyExistException(String pMensajeError) {
    super(pMensajeError);
  }
}
