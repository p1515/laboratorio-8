package excepciones;

/**
 * Clase ClientDoesNotExistException.
 * @author Francisco Javier Ovares Rojas.
 */
public class ClientDoesNotExistException extends Exception{
  /** Constructor de Excepcion. Sin parametros. */
  public ClientDoesNotExistException() {}
  /**
   *  Constructor de Excepcion. Con parametros.
   * @param pMensajeError String que representa el mensaje de error personalizado a mostrar en consola.
   */
  public ClientDoesNotExistException(String pMensajeError) {
    super(pMensajeError);
  }
}
