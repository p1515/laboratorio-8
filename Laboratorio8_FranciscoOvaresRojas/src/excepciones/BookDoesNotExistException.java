package excepciones;

/**
 * Clase BookDoesNotExistException.
 * @author Francisco Javier Ovares Rojas.
 */
public class BookDoesNotExistException extends Exception{
  /** Constructor de Excepcion. Sin parametros. */
  public BookDoesNotExistException() {}
  /**
   *  Constructor de Excepcion. Con parametros.
   * @param pMensajeError String que representa el mensaje de error personalizado a mostrar en consola.
   */
  public BookDoesNotExistException(String pMensajeError) {
    super(pMensajeError);
  }
}
