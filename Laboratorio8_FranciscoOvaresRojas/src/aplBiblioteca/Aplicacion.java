package aplBiblioteca;

import excepciones.*;
import logicaDeNegocios.Autor;
import logicaDeNegocios.Biblioteca;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Clase Aplicacion.
 * @author Francisco Javier Ovares Rojas.
 */
public class Aplicacion {
  /** Objeto Biblioteca */
  private static Biblioteca miBiblioteca = new Biblioteca("Kafra Books");

  /**
   * Funcion principal que corre el sistema.
   * @param args Args
   */
  public static void main(String[] args) {
    try {
      ejercutaAccionesBiblioteca(miBiblioteca);
    } catch (InputMismatchException e) {
      System.out.println("Debe ingresar una opción válida.");
    }
  }

  /**
   * Menu con las funciones de la clase Biblioteca implementadas.
   * @param pBiblioteca Biblioteca que representa la biblioteca a administrar.
   * @throws InputMismatchException Exception que sucede si ingresan un valor invalido.
   */
  public static void ejercutaAccionesBiblioteca(Biblioteca pBiblioteca) throws InputMismatchException{
    int choice;

    do {
      Scanner entradaEscaner = new Scanner(System.in);
      System.out.println(" |---| Bienvenido a " + pBiblioteca.getNombre() + " |---|");
      System.out.println("1. Registrar Libro.");
      System.out.println("2. Registrar Autor.");
      System.out.println("3. Registrar Cliente.");
      System.out.println("4. Prestar Libro a Cliente.");
      System.out.println("5. Buscar Libro.");
      System.out.println("6. Buscar Autor.");
      System.out.println("7. Buscar Cliente.");
      System.out.println("8. Salir del sistema.");
      System.out.println("¿Que accion desea realizar? : ");
      try {
        choice = entradaEscaner.nextInt();
        switch (choice) {
          case 1:
            try {
              registrarLibro(pBiblioteca);
            } catch (AutorDontExistException e) {
              System.out.println("El autor no se encuentra registrado. Libro no registrado.");
            } catch (BookAlreadyExistException e) {
              System.out.println("El Libro ya existe.");
            }
            break;
          case 2:
            try {
              registrarAutor(pBiblioteca);
            } catch (AuthorAlreadyExistException e) {
              System.out.println("El Autor ya esta registrado.");
            }
            break;
          case 3:
            try {
              registrarCliente(pBiblioteca);
            } catch (ClientAlreadyExistException e) {
              System.out.println("El Cliente ya está registrado.");
            }
            break;
          case 4:
            try {
              prestarLibro(pBiblioteca);
            } catch (BookDoesNotExistException e) {
              System.out.println("El libro no está registrado.");
            } catch (ClientDoesNotExistException e) {
              System.out.println("El cliente no está registrado.");
            }
            break;
          case 5:
            try{
              buscarLibro(pBiblioteca);
            } catch (BookDoesNotExistException e) {
              System.out.println("El Libro no esta registrado.");
            }
            break;
          case 6:
            try {
              buscarAutor(pBiblioteca);
            } catch (AutorDontExistException e) {
              System.out.println("El Autor no está registrado.");
            }
            break;
          case 7:
            try {
              buscarCliente(pBiblioteca);
            } catch (ClientDoesNotExistException e) {
              System.out.println("El Cliente no esta registrado.");
            }
            break;
          case 8:
            System.out.println("Volviendo al menú principal...\n");
            break;
        }
      } catch (InputMismatchException e) {
        choice = 9;
        System.out.println("Debe ingresar una acción válida.");
      }
    } while (choice != 8);

  }

  /**
   * Registra un Libro en la Biblioteca.
   * @param pBiblioteca Biblioteca que representa el objeto Biblioteca donde sera registrado el Libro.
   * @throws AutorDontExistException Exception que sucede si el Autor no esta registrado.
   * @throws BookAlreadyExistException Exception que sucede si el Libro esta registrado.
   */
  public static void registrarLibro(Biblioteca pBiblioteca) throws AutorDontExistException, BookAlreadyExistException {
    Scanner entradaEscaner = new Scanner(System.in);
    System.out.println("|---| Registrando Libro |---|");
    System.out.println("Ingrese el nombre del Libro: ");
    String nombre = entradaEscaner.nextLine();
    System.out.println("Ingrese el número identificador del Libro: ");
    int identificadorLibro = entradaEscaner.nextInt();
    System.out.println("Ingrese el identificador del Autor del Libro: ");
    int idAutor = entradaEscaner.nextInt();
    if (pBiblioteca.existeLibro(identificadorLibro)) {
      throw new BookAlreadyExistException();
    }
    if (pBiblioteca.existeAutor(idAutor) && !pBiblioteca.existeLibro(identificadorLibro)) {
      Autor tempAutor = pBiblioteca.buscarAutor(idAutor);
      pBiblioteca.registrarLibro(nombre, identificadorLibro, tempAutor);
      System.out.println("Libro registrado con éxtio.");
    } else {
      throw new AutorDontExistException();
    }

  }

  /**
   * Registra un Autor en la Biblioteca.
   * @param pBiblioteca Biblioteca que representa el objeto Biblioteca donde sera registrado el Autor.
   * @throws AuthorAlreadyExistException Exception que sucede si el Autor esta registrado.
   */
  public static void registrarAutor(Biblioteca pBiblioteca) throws AuthorAlreadyExistException{
    Scanner entradaEscaner = new Scanner(System.in);
    System.out.println("|---| Registrando Autor |---|");
    System.out.println("Ingrese el nombre del Autor: ");
    String nombre = entradaEscaner.nextLine();
    System.out.println("Ingrese el número identificador del Autor: ");
    int identificador = entradaEscaner.nextInt();
    if (pBiblioteca.existeAutor(identificador)) {
      throw new AuthorAlreadyExistException();
    } else {
      pBiblioteca.registrarAutor(identificador, nombre);
      System.out.println("Autor regristrado con exito.");
    }

  }

  /**
   * Registra un Cliente en la Biblioteca.
   * @param pBiblioteca Biblioteca que representa el objeto Biblioteca donde sera registrado el Cliente.
   * @throws ClientAlreadyExistException Exception que sucede si el Cliente esta registrado.
   */
  public static void registrarCliente(Biblioteca pBiblioteca) throws ClientAlreadyExistException {
    Scanner entradaEscaner = new Scanner(System.in);
    System.out.println("|---| Registrando Cliente |---|");
    System.out.println("Ingrese el nombre del Cliente: ");
    String nombre = entradaEscaner.nextLine();
    System.out.println("Ingrese el número identificador del Cliente: ");
    String identificador = entradaEscaner.nextLine();
    System.out.println("Ingrese la dirección del Cliente: ");
    String direccion = entradaEscaner.nextLine();
    if (pBiblioteca.existeCliente(identificador)) {
      throw new ClientAlreadyExistException();
    } else {
      pBiblioteca.registrarCliente(nombre, identificador, direccion);
      System.out.println("Cliente registrado con éxito.\n");
    }
  }

  /**
   * Registra un objeto libro en un objeto Cliente y un objeto Prestamo en la Biblioteca.
   * @param pBiblioteca Biblioteca que representa el objeto Biblioteca donde sera registrado el prestamo.
   * @throws BookDoesNotExistException Exception que sucede si el Libro no esta regisrado.
   * @throws ClientDoesNotExistException Exception que sucede si el Cliente no está registrado.
   */
  public static void prestarLibro(Biblioteca pBiblioteca) throws BookDoesNotExistException, ClientDoesNotExistException {
    Scanner entradaEscaner = new Scanner(System.in);
    System.out.println("|---| Buscando Libro |---|");
    System.out.println("Ingrese el número identificador del Libro: ");
    int identificadorLibro = entradaEscaner.nextInt();
    if (!pBiblioteca.existeLibro(identificadorLibro)) {
      throw new BookDoesNotExistException();
    }
    Scanner entradaNueva = new Scanner(System.in);
    System.out.println("Ingrese el numero identificador del Cliente interesado: ");
    String identificadorCliente = entradaNueva.nextLine();
    if (!pBiblioteca.existeCliente(identificadorCliente)) {
      throw new ClientDoesNotExistException();
    }
    pBiblioteca.buscarCliente(identificadorCliente).registrarPrestamo(pBiblioteca.buscarLibro(identificadorLibro));
    System.out.println("Libro prestado con exito.");
  }

  /**
   * Busca un Libro en la Biblioteca.
   * @param pBiblioteca Biblioteca que representa el objeto Biblioteca donde sera buscado el Libro.
   * @throws BookDoesNotExistException Exception que sucede si el Libro no esta registrado.
   */
  public static void buscarLibro(Biblioteca pBiblioteca) throws BookDoesNotExistException {
    Scanner entradaEscaner = new Scanner(System.in);
    System.out.println("|---| Buscando Libro |---|");
    System.out.println("Ingrese el identificador numerico del libro: ");
    int identificador = entradaEscaner.nextInt();
    if (pBiblioteca.existeLibro(identificador)) {
      System.out.println(pBiblioteca.buscarLibro(identificador).toString());
    } else {
      throw new BookDoesNotExistException();
    }
  }

  /**
   * Busca un Autor en la Biblioteca.
   * @param pBiblioteca Biblioteca que representa el objeto Biblioteca donde sera buscado el Autor.
   * @throws AutorDontExistException Exception que sucede si el Autor no esta registrado.
   */
  public static void buscarAutor(Biblioteca pBiblioteca) throws AutorDontExistException {
    Scanner entradaEscaner = new Scanner(System.in);
    System.out.println("|---| Buscando Autor |---|");
    System.out.println("Ingrese el identificador numerico del Autor: ");
    int identificador = entradaEscaner.nextInt();
    if (pBiblioteca.existeAutor(identificador)) {
      System.out.println(pBiblioteca.buscarAutor(identificador).toString());
    } else {
      throw new AutorDontExistException();
    }
  }

  /**
   * Busca un Cliente en la Biblioteca.
   * @param pBiblioteca Biblioteca que representa el objeto Biblioteca donde sera buscado el Cliente.
   * @throws ClientDoesNotExistException Exception que sucede si el Cliente no esta registrado.
   */
  public static void buscarCliente(Biblioteca pBiblioteca) throws ClientDoesNotExistException {
    Scanner entradaEscaner = new Scanner(System.in);
    System.out.println("|---| Buscando Cliente |---|");
    System.out.println("Ingrese el identificador del Cliente: ");
    String identificador = entradaEscaner.nextLine();
    if (pBiblioteca.existeCliente(identificador)) {
      System.out.println(pBiblioteca.buscarCliente(identificador).toString());
    } else {
      throw new ClientDoesNotExistException();
    }
  }
}
