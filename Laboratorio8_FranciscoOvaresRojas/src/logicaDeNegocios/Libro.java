package logicaDeNegocios;

import java.util.ArrayList;

/**
 * Clase Libro.
 * @author Francisco Javier Ovares Rojas.
 */
public class Libro {
  /** Nombre del Libro */
  private String nombre;
  /** Editorial del Libro */
  private String editorial;
  /** Año de publicación del Libro */
  private int anoPublicacion;
  /** Identificador del Libro */
  private int identificador;
  /** Autor del Libro */
  private final ArrayList<Autor> autores = new ArrayList<Autor>();
  /** Contador sobre cantidad de Libros */
  private static int cantidadEjemplares = 0;

  /** Constructor directo del Objeto Libro, sin parametros. */
  public Libro() {
    cantidadEjemplares++;
  }

  /**
   * Constructor de Objeto Libro.
   * @param pNombre String que representa el nombre del Libro.
   * @param pIdentificador int que representa el identificador del Libro.
   * @param pAutor Object que representa el Autor del Libro.
   */
  public Libro(String pNombre, int pIdentificador, Autor pAutor) {
    this.nombre = pNombre;
    this.identificador = pIdentificador;
    this.autores.add(pAutor);
    setEditorial("Moonlight Editorial");
    cantidadEjemplares++;
  }

  // Métodos accesores.
  public String getNombre() {
    return this.nombre;
  }
  public void setNombre(String pNombre) {
    this.nombre = pNombre;
  }
  public String getEditorial() {
    return this.editorial;
  }
  public void setEditorial(String pEditorial) {
    this.editorial = pEditorial;
  }
  public int getAnoPublicacion() {
    return this.anoPublicacion;
  }
  public void setAnoPublicacion(int pAno) {
    this.anoPublicacion = pAno;
  }
  public int getIdentificador() {
    return this.identificador;
  }
  public void setIdentificador(int pIdentificador) {
    this.identificador = pIdentificador;
  }
  // Fin métodos accesores.

  /**
   * Muestra los valores del objeto.
   * @return msg String que representa una cadena con la informacion.
   */
  public String toString() {
    String msg = "|- INFORMACIÓN DEL LIBRO -|" + "\n";
    msg += "Nombre: " + getNombre() + "\n";
    msg += "Editorial: " + getEditorial() + "\n";
    msg += "Año Publicado: " + getAnoPublicacion() + "\n";
    for (Autor tempAutor : this.autores) {
      msg += tempAutor.toString();
    }
    return msg;
  }
}
