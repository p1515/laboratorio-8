package logicaDeNegocios;

import java.util.*;

/**
 * Clase Biblioteca.
 * @author Francisco Javier Ovares Rojas.
 */
public class Biblioteca {
  /** Nombre de la Biblioteca */
  private String nombre;
  /** Lista de tipo Cliente */
  private final ArrayList<Cliente> clientesLista = new ArrayList<Cliente>();
  /** Lista de tipo Autor */
  private final ArrayList<Autor> autoresLista = new ArrayList<Autor>();
  /** Lista de tipo Libro */
  private final ArrayList<Libro> librosLista = new ArrayList<Libro>();

  /** Constructor del Objeto Biblioteca. Sin parámetros. */
  public Biblioteca() {}
  /**
   * Constructor del Objeto Biblioteca. Con parámetros.
   * @param pNombre String que representa el nombre de la Biblioteca.
   */
  public Biblioteca(String pNombre) {
    this.nombre = pNombre;
  }

  // Métodos accesores.
  public String getNombre() {
    return this.nombre;
  }
  public void setNombre(String pNombre) {
    this.nombre = pNombre;
  }
  public ArrayList<Cliente> getClientesLista() {
    return this.clientesLista;
  }
  public ArrayList<Autor> getAutoresLista() {
    return this.autoresLista;
  }
  public ArrayList<Libro> getLibrosLista() {
    return this.librosLista;
  }
  // Fin métodos accesores.

  /**
   * Registra un Libro.
   * @param pNombre String que representa el nombre del Libro.
   * @param pIdentificador int que representa el identificador del Libro.
   * @param pAutor Object que representa el Autor del Libro.
   */
  public void registrarLibro(String pNombre, int pIdentificador, Autor pAutor) {
    Libro nuevoLibro = new Libro(pNombre, pIdentificador, pAutor);
    this.librosLista.add(nuevoLibro);
  }

  /**
   * Registra un autor.
   * @param pIdentificador int que representa el identificador del Autor.
   */
  public void registrarAutor(int pIdentificador, String pNombre) {
    Autor nuevoAutor = new Autor(pNombre, pIdentificador);
    this.autoresLista.add(nuevoAutor);
  }

  /**
   * Registra un Cliente.
   * @param pIdentificador int que representa el identificador del Cliente.
   */
  public void registrarCliente(String pNombre, String pIdentificador, String pDireccion) {
    Cliente nuevoCliente = new Cliente(pNombre, pIdentificador, pDireccion);
    this.clientesLista.add(nuevoCliente);
  }

  /**
   * Presta un Libro a un Cliente.
   * @param pIdentificadorCliente int que representa el identificador del Cliente.
   * @param pIdentificadorLibro int que representa el identificador del Libro.
   */
  public void prestarLibro(String pIdentificadorCliente, int pIdentificadorLibro) {
    Cliente clienteInteresado = buscarCliente(pIdentificadorCliente);
    Libro libroEscogido = buscarLibro(pIdentificadorLibro);
    clienteInteresado.registrarPrestamo(libroEscogido);
  }

  /**
   * Busca un libro mediante su identificador.
   * @param pIdentificador int que representa el identificador del Libro.
   * @return libroBuscado Object que representa el Libro buscado.
   */
  public Libro buscarLibro(int pIdentificador) {
    Libro libroBuscado = new Libro();

    for (Libro tempLibro : librosLista) {
      if (tempLibro.getIdentificador() == pIdentificador) {
        libroBuscado = tempLibro;
        break;
      }
    }

    return libroBuscado;
  }

  /**
   * Busca un cliente mediante su identificador.
   * @param pIdentificador int que representa el identificador del Cliente.
   * @return clienteBuscado Object que representa el Cliente buscado.
   */
  public Cliente buscarCliente(String pIdentificador) {
    Cliente clienteBuscado = new Cliente();

    for (Cliente tempCliente : clientesLista) {
      if (tempCliente.cedula.equals(pIdentificador)) {
        clienteBuscado = tempCliente;
        break;
      }
    }

    return clienteBuscado;
  }

  /**
   * Busca un Autor mediante su identificador.
   * @param pIdentificador int que representa el identificador del Autor.
   * @return libroBuscado Object que representa el Autor buscado.
   */
  public Autor buscarAutor(int pIdentificador) {
    Autor libroBuscado = new Autor();

    for (Autor tempAutor : autoresLista) {
      if (tempAutor.getIdentificador() == pIdentificador) {
        libroBuscado = tempAutor;
      }
    }
    return libroBuscado;
  }

  /**
   * Verifica si existe un Libro.
   * @param pIdentificador int que representa el indetificador del objeto.
   * @return isLibro que representa el resultado de la busqueda.
   */
  public boolean existeLibro(int pIdentificador) {
    boolean isLibro = false;

    for (Libro tempLibro : librosLista) {
      if (tempLibro.getIdentificador() == pIdentificador) {
        isLibro = true;
        break;
      }
    }

    return isLibro;
  }

  /**
   * Verifica si existe un Autor.
   * @param pIdentificador int que representa el indetificador del objeto.
   * @return isLibro que representa el resultado de la busqueda.
   */
  public boolean existeAutor(int pIdentificador) {
    boolean isAutor = false;

    for (Autor tempAutor : autoresLista) {
      if (tempAutor.getIdentificador() == pIdentificador) {
        isAutor = true;
        break;
      }
    }

    return isAutor;
  }

  /**
   * Verifica si existe un Cliente.
   * @param pIdentificador int que representa el indetificador del objeto.
   * @return isLibro que representa el resultado de la busqueda.
   */
  public boolean existeCliente(String pIdentificador) {
    boolean isCliente = false;

    for (Cliente tempCliente : clientesLista) {
      if (tempCliente.getCedula().equals(pIdentificador)) {
        isCliente = true;
        break;
      }
    }

    return isCliente;
  }
}
