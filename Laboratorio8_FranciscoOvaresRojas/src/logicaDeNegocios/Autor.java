package logicaDeNegocios;

/**
 * Clase Autor.
 * @author Francisco Javier Ovares Rojas.
 */
public class Autor {
  /** Nombre del Autor */
  String nombre;
  /** Identificador del Autor */
  int identificador;
  /** Nacionalidad del Autor */
  String nacionalidad;

  /**
   * Constructor de Objeto Autor. Sin parametros.
   */
  public Autor() {
    this.nacionalidad = "Alemana";
  }
  /**
   * Constructor de Objeto Autor. Con parametros.
   * @param pNombre String que representa el nombre del Autor.
   * @param pIdentificador int que representa el identificador del Autor.
   */
  public Autor(String pNombre, int pIdentificador) {
    this.identificador = pIdentificador;
    this.nombre = pNombre;
    this.nacionalidad = "Alemana";
  }

  // Métodos accesores.
  public String getNombre() {
    return this.nombre;
  }
  public void setNombre(String pNombre) {
    this.nombre = pNombre;
  }
  public int getIdentificador() {
    return this.identificador;
  }
  public void setIdentificador(int pIdentificador) {
    this.identificador = pIdentificador;
  }
  public String getNacionalidad() {
    return this.nacionalidad;
  }
  public void setNacionalidad(String pNacionalidad) {
    this.nacionalidad = pNacionalidad;
  }
  // Fin métodos accesores.

  /**
   * Muestra los valores del objeto.
   * @return msg String que representa una cadena con la informacion.
   */
  public String toString() {
    String msg = "|--| Autor |--|" + "\n";
    msg += "Nombre: " + getNombre() + "\n";
    msg += "Identificador: " + getIdentificador() + "\n";
    msg += "Nacionalidad: " + getNacionalidad() + "\n";
    return msg;
  }
}
