package logicaDeNegocios;

import java.time.LocalDate;

/**
 * Clase Prestamo.
 * @author Francisco Javier Ovares Rojas.
 */
public class Prestamo {
  /** Fecha del Prestamo */
  LocalDate fechaPrestamo;
  /** Fecha de Entra del Prestamo */
  LocalDate fechaEntrega;
  /** Libro prestado */
  Libro libro;

  /**
   * Constructor de Objeto Prestamo. Con parametros.
   * @param pLibro Libro que representa el ejemplar que sera prestado.
   * @param pAno int que representa el year de Prestamo.
   * @param pMes int que representa el mes de Prestamo.
   * @param pDia int que representa el dia de Prestamo.
   */
  public Prestamo(Libro pLibro, int pAno, int pMes, int pDia) {
    setFechaPrestamo(pAno, pMes, pDia);
    setFechaEntrega(pAno, pMes, pDia + 15);
    setLibro(pLibro);
  }
  /**
   * Constructor de Objeto Prestamo. Con parametros.
   * @param pLibro Libro que representa el ejemplar que sera prestado.
   */
  public Prestamo(Libro pLibro) {
    this.libro = pLibro;
    this.fechaPrestamo = LocalDate.now();
    int dia = LocalDate.now().getDayOfMonth() + 15;
    this.fechaEntrega = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), dia);
  }

  // Métodos accesores.
  public LocalDate getFechaPrestamo() {
    return this.fechaPrestamo;
  }
  public void setFechaPrestamo(int pAno, int pMes, int pDia) {
    this.fechaPrestamo = LocalDate.of(pAno, pMes, pDia);
  }
  public LocalDate getFechaEntrega() {
    return this.fechaEntrega;
  }
  public void setFechaEntrega(int pAno, int pMes, int pDia) {
    this.fechaEntrega = LocalDate.of(pAno, pMes, pDia);
  }
  public Libro getLibro() {
    return this.libro;
  }
  public void setLibro(Libro pLibro) {
    this.libro = pLibro;
  }
  // Fin métodos accesores.

  /**
   * Muestra los valores del objeto.
   * @return msg String que representa una cadena con la informacion.
   */
  public String toString() {
    String msg = "|- INFORMACIÓN DE LOS PRÉSTAMOS -|" + "\n";
    msg += "Fecha de Prestamo: " + getFechaPrestamo() + "\n";
    msg += "Fecha de Entrega: " + getFechaEntrega() + "\n";
    msg += "Libro prestado: " + getLibro().toString() + "\n";
    return msg;
  }
}
