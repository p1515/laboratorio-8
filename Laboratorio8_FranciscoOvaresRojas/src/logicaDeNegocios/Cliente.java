package logicaDeNegocios;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase Cliente.
 * @author Francisco Javier Ovares Rojas.
 */
public class Cliente {
  /** Nombre del Cliente */
  String nombre;
  /** Cédula del Cliente */
  String cedula;
  /** Dirección del Cliente */
  String direccion;
  /** Préstamos del Cliente */
  ArrayList<Prestamo> prestamos = new ArrayList<Prestamo>();

  /** Constructor de Objeto Cliente. No necesita parametros. */
  public Cliente() {};

  /**
   * Constructor de Objeto Cliente. Con parametros.
   * @param pNombre String que representa el nombre del Cliente.
   * @param pCedula String que representa la cedula del Cliente.
   * @param pDireccion String que representa la direccion del Cliente.
   */
  public Cliente(String pNombre, String pCedula, String pDireccion) {
    this.nombre = pNombre;
    this.cedula = pCedula;
    this.direccion = pDireccion;
  };

  /**
   * Registra un nuevo prestamo al Cliente.
   * @param pLibro Object que representa el Libro que sera prestado.
   */
  public void registrarPrestamo(Libro pLibro) {
    Prestamo nuevoPrestamo = new Prestamo(pLibro);
    this.prestamos.add(nuevoPrestamo);
  }

  /**
   * Busca libros con entrega atrasada y los add a un Array.
   * @return librosAtrasados ArrayList<Libro> que representa los libros atrasados.
   */
  public ArrayList<Libro> librosConAtraso() {
    ArrayList<Libro> librosAtrasados = new ArrayList<Libro>();

    for (Prestamo tempPrestamo : prestamos) {
      if (tempPrestamo.getFechaEntrega().isAfter(LocalDate.now())) {
        librosAtrasados.add(tempPrestamo.getLibro());
      }
    }

    return  librosAtrasados;
  }

  // Métodos accesores.
  public String getCedula() {
    return this.cedula;
  }
  public String getNombre() {
    return this.nombre;
  }
  public String getDireccion() {
    return this.direccion;
  }
  // Fin métodos accesores.

  /**
   * Muestra los valores del objeto.
   * @return msg String que representa una cadena con la informacion.
   */
  public String toString() {
    String msg = "|- INFORMACIÓN DEL Cliente -|" + "\n";
    msg += "Nombre: " + getNombre() + "\n";
    msg += "Identificador: " + getCedula() + "\n";
    msg += "Dirección: " +getDireccion() + "\n";
    for(Prestamo tempPrestamos : this.prestamos) {
      msg += tempPrestamos.getLibro().toString();
    }
    return msg;
  }
}
